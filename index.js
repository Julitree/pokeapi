let containerPokemon = document.getElementById("imgContainer");
let imgContainer = document.getElementById("imgContainer");

function getInputValue() {
    let inputVal = document.getElementById("myInput").value;
    fetch(`https://pokeapi.co/api/v2/pokemon/${inputVal}`)
        .then(response => {
            return response.json();
        })
        .then(myJson => {
            imgContainer.innerHTML = `<h1>${myJson.name}</h1>
                                     <div class="imgContainer">
                                      <img src=${myJson.sprites.front_default}>
                                      </div>`
        })
        .catch(error => {
            imgContainer.innerHTML = `<h1>${inputVal} It's not a Pokemon</h1>`
        })
}